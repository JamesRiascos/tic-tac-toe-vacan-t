//Imports
var express = require('express')
var app = express()
app.use(express.static('public'))
const port = process.env.PORT || 3000
var http = require('http').createServer(app);
var io = require('socket.io')(http);


http.listen(port)

//ruta
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/tictac.html');
});

//
var players = {},
    unmatched;

//Conexión al Socket
io.sockets.on("connection", function(socket) {
    console.log("Socket Conectado")
    ticttacJuego(socket);

    if (getOpponent(socket)) {
        socket.emit("game.begin", {
            symbol: players[socket.id].symbol,
        });
        getOpponent(socket).emit("game.begin", {
            symbol: players[getOpponent(socket).id].symbol,
        });
    }

    socket.on("make.move", function(data) {
        if (!getOpponent(socket)) {
            return;
        }
        socket.emit("move.made", data);
        getOpponent(socket).emit("move.made", data);
    });

    socket.on("disconnect", function() {
        if (getOpponent(socket)) {
            getOpponent(socket).emit("opponent.left");
        }
    });
});

function ticttacJuego(socket) {
    players[socket.id] = {
        opponent: unmatched,

        symbol: "X",
        // El socket está asociado con este Jugador
        socket: socket,
    };
    if (unmatched) {
        players[socket.id].symbol = "O";
        players[unmatched].opponent = socket.id;
        unmatched = null;
    } else {
        unmatched = socket.id;
    }
}

function getOpponent(socket) {
    if (!players[socket.id].opponent) {
        return;
    }
    return players[players[socket.id].opponent].socket;
}