var socket = io();
var symbol;
$(function() {
    $(".board button").attr("disabled", true);
    $(".board> button").on("click", makeMove);
    // Se llama el evento cuando algún jugador hace un movimiento

    socket.on("move.made", function(data) {

        // Se hace el movimiento
        $("#" + data.position).text(data.symbol);

        // Si el símbolo es el mismo que el del jugador, podemos asumir que es su turno
        myTurn = data.symbol !== symbol;

        // Durante el juego, se muestra de quien es el turno
        if (!isGameOver()) {
            if (gameTied()) {
                $("#messages").text("Empate!");
                $(".board button").attr("disabled", true);
            } else {
                renderTurnMessage();
            }
            // Cuando el juego termina
        } else {
            // Se muestra el mensaje al perdedor
            if (myTurn) {
                $("#messages").text("Fin del Juego. Has perdido.");
                // Se muestra mensaje al ganador
            } else {
                $("#messages").text("Fin del Juego. Has Ganado!");
            }
            // Deshabilito el tablero
            $(".board button").attr("disabled", true);
        }
    });

    // Se configura estado inicial cuando empieza el juego
    socket.on("game.begin", function(data) {
        // El servidor asigna X o O al jugador
        symbol = data.symbol;
        // Primer turno
        myTurn = symbol === "X";
        renderTurnMessage();
    });

    // Se deshabilita el tablera si el otro jugador se retira
    socket.on("opponent.left", function() {
        $("#messages").text("Se ha retirado tu oponente.");
        $(".board button").attr("disabled", true);
    });
});

function getBoardState() {
    var obj = {};
    // Se compone un objeto con todas las X y O que están en el tablero
    $(".board button").each(function() {
        obj[$(this).attr("id")] = $(this).text() || "";
    });
    return obj;
}

function gameTied() {
    var state = getBoardState();

    if (
        state.a0 !== "" &&
        state.a1 !== "" &&
        state.a2 !== "" &&
        state.b0 !== "" &&
        state.b1 !== "" &&
        state.b2 !== "" &&
        state.b3 !== "" &&
        state.c0 !== "" &&
        state.c1 !== "" &&
        state.c2 !== ""
    ) {
        return true;
    }
}

// En esta función se valida cuando y como termina el juego
function isGameOver() {
    var state = getBoardState(),
        // cualquiera de las filas debe ser igual a cualquiera de estas(matches = ["XXX", "OOO"]) y el juego termina
        matches = ["XXX", "OOO"],
        // Estas son las combinaciones posible y así se ganaría el juego
        rows = [
            state.a0 + state.a1 + state.a2,
            state.b0 + state.b1 + state.b2,
            state.c0 + state.c1 + state.c2,
            state.a0 + state.b1 + state.c2,
            state.a2 + state.b1 + state.c0,
            state.a0 + state.b0 + state.c0,
            state.a1 + state.b1 + state.c1,
            state.a2 + state.b2 + state.c2,
        ];

    // Para cualquiera de los jugadores 'XXX' or 'OOO'
    for (var i = 0; i < rows.length; i++) {
        if (rows[i] === matches[0] || rows[i] === matches[1]) {
            return true;
        }
    }
}

// Aquí se valida el turno de los jugadores para habilitar o deshabilitar el tablero segun el turno
function renderTurnMessage() {
    // Deshabilita el tablero si no le corresponde el turno
    if (!myTurn) {
        $("#messages").text("Es el turno de tu oponente");
        $(".board button").attr("disabled", true);
        // Habilita el tablero a quien corresponde el turno
    } else {
        $("#messages").text("Tu turno");
        $(".board button").removeAttr("disabled");
    }
}

// En esta funcion se valida el si es un turno u otro y valida si una casilla se encuentra seleccionada o libre
function makeMove(e) {
    e.preventDefault();
    // Valida si es mi turno
    if (!myTurn) {
        return;
    }
    // Valida si se ha seleccionado una casilla
    if ($(this).text().length) {
        return;
    }

    // Indica al servidor el movimiento
    socket.emit("make.move", {
        symbol: symbol,
        position: $(this).attr("id"),
    });
}